extends Control


signal change_view_requested


export var LINES_IN_PRIZE_COLUMN: int = 3
var REPLAY_NUMBER: int = 1
var SCORE: int = 0 setget set_score
var COMBINATIONS: Array
var payload: Dictionary = {} setget set_payload


# BUILTINS -------------------------


func _ready() -> void:
	var _c: int
	_c = ($Board as TextureRect).connect("board_ckecked", self, "_on_Board_board_ckecked")
	_c = ($Lines as Control).connect("lines_drawing_ended", self, "_on_Lines_lines_drawing_ended")
	_generate_prize_grid()
	_generate_serial()
	set_score(SCORE)


# METHODS -------------------------


func _generate_prize_column(props: Array) -> Label:
	var column_label: Label = Label.new()
	column_label.set("custom_colors/font_color", Color.black)
	column_label.set("custom_fonts/font", load("res://Resourses/Fonts/PrizeFont.tres"))
	for i in props:
		column_label.text += "%s — %s\n" % [i.value, i.cost if i.cost < 1000 else str(i.cost / 1000) + "K"]
	column_label.rect_size = Vector2.ZERO
	return column_label


func _generate_prize_grid() -> void:
	var column: Array = []
	for i in Global.get("NUMBERS"):
		column.append(i)
		if fmod(i.value, LINES_IN_PRIZE_COLUMN) == 0:
			($Barcode/PrizeGrid as BoxContainer).add_child(_generate_prize_column(column))
			column.clear()


func _generate_serial() -> void:
	var replay_number: String = "0".repeat(3 - str(REPLAY_NUMBER).length()) + str(REPLAY_NUMBER)
	var rn: int = randi() % 1000
	var rn_str: String = "0".repeat(3 - str(rn).length()) + str(rn)
	var date: Dictionary = OS.get_date()
	var month_str: String = "0" + str(date.month) if date.month < 10 else str(date.month)
	var day_str: String = "0" + str(date.day) if date.day < 10 else str(date.day)
	var year_str: String = str(date.year).substr(2)
	($Barcode/SerialDate as Label).text = "%s-%s-%s       %s/%s/%s" % [replay_number, OS.get_unique_id(), rn_str, month_str, day_str, year_str]


# SETGET -------------------------


func set_score(score: int) -> void:
	SCORE += score
	($Score as Label).text = Global.call("format_number", SCORE)


func set_payload(pl: Dictionary) -> void:
	payload = pl
	($Board as TextureRect).call_deferred("prepare_board")
	($Lines as Control).call_deferred("clear_lines")
	REPLAY_NUMBER += 1
	_generate_serial()
	set_score(payload.round_score)


# SIGNALS -------------------------


func _on_Board_board_ckecked(combinations: Array) -> void:
	COMBINATIONS = combinations
	($Lines as Control).call_deferred("draw_lines", combinations)


func _on_Lines_lines_drawing_ended() -> void:
	emit_signal("change_view_requested", Global.get("VIEW_MAP").SUMMARY, { "combinations": COMBINATIONS, "score": SCORE })


