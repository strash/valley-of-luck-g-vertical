extends TextureRect


signal board_ckecked


const Number: PackedScene = preload("res://Scenes/Components/Number.tscn")
const ERASER_MASK: Texture = preload("res://Resourses/Sprites/eraser_mask_lookup.png")
onready var REMOVABLE_IMAGE_SRC: Texture = ($RemovableLayer as TextureRect).texture
# массив векторов, в которых x, y - позиция пиксела
# в нем хранятся только те пиксели, в которых альфа равна 0
# массив уменьшен для оптимизации и хранит только четные пиксели
# проще говоря - маска
var eraser_positions_alfa: PoolVector2Array = PoolVector2Array([])
var removable_image: Image = Image.new()
var removable_image_texture: ImageTexture = ImageTexture.new()

# сколько нужно стереть, чтобы получить результат
const MIN_ERASED_PERCENTAGE: float = 0.92
# количество пикселей на удаляемой картинке
var REMOVABLE_IMAGE_PIXEL_COUNT: int

# минимальное количество схожих цифр для выигрыша
const MIN_NUMBER_OF_MATCHES: int = 3
# количество цифр в сетке
const NUMBERS_COUNT: int = 25

var is_board_locked: bool = false


# BUILTINS -------------------------


func _ready() -> void:
	prepare_board()
	_generate_numbers_to_board()
	REMOVABLE_IMAGE_PIXEL_COUNT = removable_image.get_width() * removable_image.get_height()
	_eraser_lookup()


func _gui_input(event) -> void:
	if not is_board_locked:
		if event is InputEventScreenTouch and event.is_pressed() or event is InputEventScreenDrag:
			_erase_image(event.position)
			_update_removable_image()
		elif event is InputEventScreenTouch and not event.is_pressed():
			_removable_image_lookup()


# METHODS -------------------------


func prepare_board() -> void:
	# восстанавлваем исходную картинку
	($RemovableLayer as TextureRect).texture = REMOVABLE_IMAGE_SRC
	removable_image = REMOVABLE_IMAGE_SRC.get_data()
	removable_image.convert(Image.FORMAT_RGBA8)
	is_board_locked = false
	if ($NumbersGrid as GridContainer).get_child_count() > 0:
		for i in ($NumbersGrid as GridContainer).get_children():
			i.call("update_self")


func _check_board() -> void:
	var combinations: Array = []
	var num_size: Vector2 = (($NumbersGrid as GridContainer).get_child(0) as Label).get_rect().size
	for i in ($NumbersGrid as GridContainer).get_children():
		var num_pos_center: Vector2 = i.get_global_rect().position + num_size / 2.0
		var count: int = 0
		if not combinations.empty():
			for j in combinations:
				if i.get("id") == j.id:
					j.pos_center.append(num_pos_center)
					count += 1
		if combinations.empty() or count == 0:
			var combination: Dictionary = {}
			combination.id = i.get("id")
			combination.value = i.get("value")
			combination.cost = i.get("cost")
			combination.pos_center = []
			combination.pos_center.append(num_pos_center)
			combinations.append(combination)
	emit_signal("board_ckecked", _clean_combinations(combinations))


func _clean_combinations(combinations: Array) -> Array:
	var cleaned_combinations: Array = []
	for i in combinations:
		if i.pos_center.size() >= MIN_NUMBER_OF_MATCHES:
			i.count = i.pos_center.size() - (MIN_NUMBER_OF_MATCHES - 1)
			cleaned_combinations.append(i)
	return cleaned_combinations


func _generate_numbers_to_board() -> void:
	for i in NUMBERS_COUNT:
		var number: Label = Number.instance()
		($NumbersGrid as GridContainer).add_child(number)


func _removable_image_lookup() -> void:
	var count: float = 0.0
	removable_image.lock()
	for x in removable_image.get_width():
		for y in removable_image.get_height():
			if removable_image.get_pixel(x, y).a == 0:
				count += 1.0
	removable_image.unlock()
	if count / REMOVABLE_IMAGE_PIXEL_COUNT >= MIN_ERASED_PERCENTAGE:
		_check_board()
		is_board_locked = true


func _eraser_lookup() -> void:
	var eraser_mask_image: Image = ERASER_MASK.get_data()
	eraser_mask_image.lock()
	var e_w: int = eraser_mask_image.get_width()
	var e_h: int = eraser_mask_image.get_height()
	var e_w_off: int = int(floor(e_w / 2.0))
	var e_h_off: int = int(floor(e_h / 2.0))
	for x in e_w:
		for y in e_h:
			var pixel: Color = eraser_mask_image.get_pixel(x, y)
			# чтобы через 1 пиксель была маска
			# if pixel.a == 0.0 and fmod(x, 2) == 0 and fmod(y, 2) == 0:
			if pixel.a == 0.0:
				eraser_positions_alfa.append(Vector2(x - e_w_off, y - e_h_off))
	eraser_mask_image.unlock()


func _erase_image(pos: Vector2) -> void:
	removable_image.lock()
	for i in eraser_positions_alfa:
		var x: int = int(pos.x) + i.x
		var y: int = int(pos.y) + i.y
		if _is_pixel_inside_the_image(x, y):
			removable_image.set_pixel(x, y, Color(1.0, 1.0, 1.0, 0.0))
	removable_image.unlock()


func _is_pixel_inside_the_image(x: int, y: int) -> bool:
	if x >= 0 and y >= 0 and x < removable_image.get_width() and y < removable_image.get_height():
		return true
	return false


func _update_removable_image() -> void:
	removable_image_texture.create_from_image(removable_image)
	($RemovableLayer as TextureRect).texture = removable_image_texture


# SETGET -------------------------


# SIGNALS -------------------------


