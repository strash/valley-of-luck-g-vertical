extends Control


signal lines_drawing_ended


var is_draw_lines_allowed: bool = false

# исходный массив со всеми номерами и их координатами
var COMBINATIONS: Array = []
# скорость отрисовки
const DRAWING_SPEED: float = 3.0
export var COLOR: Color = Color8(255, 54, 54)
# таймер отрисовки. если больше 1.0, то обнуляется
var time: float = 0.0
# индекс номера, для которого в данный момент отрисовываются линии
var number: int = 0
# индекс стартовой координаты линии
var start_coordinate: int = 0
# динамический массив номеров и их координат
var numbers_coordinates: Array = []


# BUILTINS -------------------------


func _draw() -> void:
	if is_draw_lines_allowed:
		for i in numbers_coordinates:
			for j in i.size():
				if i.size() > j + 1:
					draw_line(i[j], i[j + 1], COLOR, 3, true)
	else:
		draw_rect(self.get_rect(), ColorN("white", 0.0))


func _process(delta) -> void:
	if is_draw_lines_allowed:
		_animate_drawing(delta)


# METHODS -------------------------


func draw_lines(combinations) -> void:
	COMBINATIONS = combinations
	is_draw_lines_allowed = true


func _animate_drawing(delta: float) -> void:
	time += delta * DRAWING_SPEED
	if time > 1.0:
		time = 0.0
		start_coordinate += 1
	# проверяем есть ли следующая координата
	if COMBINATIONS[number].pos_center.size() > start_coordinate + 1:
		var start: Vector2 = COMBINATIONS[number].pos_center[start_coordinate]
		var end: Vector2 = COMBINATIONS[number].pos_center[start_coordinate + 1]
		var interpolated_end: Vector2 = start.linear_interpolate(end, clamp(time, 0.0, 1.0))
		# если начали следующий тип или только начали отрисовку, то добавляем начальные координаты
		if numbers_coordinates.size() == number:
			numbers_coordinates.append([])
			numbers_coordinates[number].append(start)
			numbers_coordinates[number].append(interpolated_end)
		# проверяем какая линия сейчас отрисовывается. и если текущая, то обновляем координаты
		if numbers_coordinates[number].size() == start_coordinate + 2:
			numbers_coordinates[number][start_coordinate + 1] = interpolated_end
		else:
			numbers_coordinates[number][numbers_coordinates[number].size() - 1] = start
			numbers_coordinates[number].append(interpolated_end)
		update()
	# если точки кончились
	else:
		# переходим к следующему типу и начинаем с первой линии
		number += 1
		start_coordinate = 0
		# если типы закончились, то останавливаем отрисовку
		if COMBINATIONS.size() == number:
			is_draw_lines_allowed = false
			emit_signal("lines_drawing_ended")


func clear_lines() -> void:
	is_draw_lines_allowed = false
	update()
	numbers_coordinates.clear()
	number = 0
	start_coordinate = 0


# SETGET -------------------------


# SIGNALS -------------------------


