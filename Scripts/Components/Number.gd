extends Label


var id: int
var value: int
var cost: int


# BUILTINS -------------------------


func _ready() -> void:
	init_self()


# METHODS -------------------------


func init_self() -> void:
	var random_number: int = randi() % Global.get("NUMBERS").size()
	var random_params: Dictionary = Global.get("NUMBERS")[random_number]
	if random_params.has("value"):
		self.id = random_params.value
		self.value = random_params.value
		self.text = str(random_params.value)
	if random_params.has("cost"):
		self.cost = random_params.cost


func update_self() -> void:
	var random_number: int = randi() % Global.get("NUMBERS").size()
	var random_params: Dictionary = Global.get("NUMBERS")[random_number]
	if random_params.has("value"):
		self.id = random_params.value
		self.value = random_params.value
		self.text = str(random_params.value)
	if random_params.has("cost"):
		self.cost = random_params.cost


func remove_number() -> void:
	self.queue_free()


# SETGET -------------------------


# SIGNALS -------------------------


