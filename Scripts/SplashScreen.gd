extends Control


signal change_view_requested


# BUILTINS -------------------------


func _ready() -> void:
	($Particles2D as Particles2D).emitting = true


# METHODS -------------------------


# SETGET -------------------------


# SIGNALS -------------------------


func _on_BtnPlay_pressed() -> void:
	emit_signal("change_view_requested", Global.get("VIEW_MAP").GAME, {})


