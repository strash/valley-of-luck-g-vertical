extends Control


signal remove_view_requested


var SCORE: int = 0 setget set_score
var ROUND_SCORE: int = 0 setget set_round_score
var payload: Dictionary = {} setget set_payload

var btn_offset: float

const TWEEN_SPEED: float = 1.5
var _i: bool


# BUILTINS -------------------------


func _ready() -> void:
	btn_offset = ($BtnReplay as Button).rect_size.y
	($BtnReplay as Button).rect_position.y += btn_offset


# METHODS -------------------------


func set_score_label(score: int) -> void:
	($Score as Label).text = Global.call("format_number", score)


func set_round_score_label(round_score: int) -> void:
	($RoundScore as Label).text = "+" + Global.call("format_number", round_score)


func _calculate_round_score(combitation: Array) -> void:
	var rnd_score: int = 0
	for i in combitation:
		rnd_score += i.cost * i.count
	set_round_score(ROUND_SCORE + rnd_score)


func _generate_prizelist() -> void:
	for i in Global.get("NUMBERS"):
		($SummaryTable/PrizeList as Label).text += "%s — %s\n" % [i.value, i.cost if i.cost < 1000 else str(i.cost / 1000) + "K"]
		var j: int = 0
		var count: int = -1
		while j < payload.combinations.size():
			if payload.combinations[j].value == i.value:
				count = payload.combinations[j].count
				break
			j += 1
		if count != -1:
			($SummaryTable/CefList as Label).text += "x%s\n" % payload.combinations[j].count
		else:
			($SummaryTable/CefList as Label).text += "\n"



# SETGET -------------------------


func set_score(round_score: int) -> void:
	SCORE += round_score
	_i = ($Tween as Tween).interpolate_method(self, "set_score_label", SCORE - round_score, SCORE, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_i = ($Tween as Tween).start()
	yield($Tween as Tween, "tween_all_completed")
	_i = ($Tween as Tween).interpolate_property($RoundScore as Label, "modulate:a", null, 0.0, TWEEN_SPEED / 3.0)
	_i = ($Tween as Tween).interpolate_property($BtnReplay as Button, "rect_position:y", null, ($BtnReplay as Button).rect_position.y - btn_offset, TWEEN_SPEED / 3.0)
	if not ($Tween as Tween).is_active():
		_i = ($Tween as Tween).start()


func set_round_score(round_score: int) -> void:
	ROUND_SCORE = round_score
	_i = ($Tween as Tween).interpolate_method(self, "set_round_score_label", 0, ROUND_SCORE, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_i = ($Tween as Tween).start()
	yield($Tween as Tween, "tween_all_completed")
	set_score(ROUND_SCORE)


func set_payload(pl: Dictionary) -> void:
	payload = pl
	SCORE = payload.score
	set_score_label(SCORE)
	_generate_prizelist()
	yield(get_tree().create_timer(0.3), "timeout")
	_calculate_round_score(payload.combinations)


# SIGNALS -------------------------


func _on_BtnReplay_pressed() -> void:
	emit_signal("remove_view_requested", { "round_score": ROUND_SCORE })


