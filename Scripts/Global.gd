extends Node


var REGEX: RegEx = RegEx.new()
const _regex_string: String = "(\\d)(?=(\\d\\d\\d)+([^\\d]|$))"
var _is_regex_ok: bool = false


# енум имен сцен
enum VIEW_MAP {
	SPLASH,
	GAME,
	SUMMARY,
}
# имена всех сцен
const VIEWS: PoolStringArray = PoolStringArray([
	"res://Scenes/SplashScreen.tscn",
	"res://Scenes/GameScreen.tscn",
	"res://Scenes/SummaryScreen.tscn",
])


const NUMBERS: Array = [
	{
		"cost": 100,
		"value": 1,
	},
	{
		"cost": 1_000,
		"value": 2,
	},
	{
		"cost": 5_000,
		"value": 3,
	},
	{
		"cost": 10_000,
		"value": 4,
	},
	{
		"cost": 20_000,
		"value": 5,
	},
	{
		"cost": 50_000,
		"value": 6,
	},
	{
		"cost": 100_000,
		"value": 7,
	},
	{
		"cost": 200_000,
		"value": 8,
	},
	{
		"cost": 500_000,
		"value": 9,
	},
]


# BUILTINS -------------------------


func _init() -> void:
	_is_regex_ok = true if REGEX.compile(_regex_string) == OK else false


# METHODS -------------------------


# форматирование числа, проставление знаков между порядками
func format_number(number: int, delimiter: String = ",") -> String:
	var number_string: String = str(number)
	if _is_regex_ok:
		return REGEX.sub(number_string, "$1" + delimiter, true)
	else:
		return number_string


# SETGET -------------------------


# SIGNALS -------------------------


