extends Control


onready var STACK_CONTAINER: Control = $Stack as Control

# стэк вьюх состоящий из имен вьюх
var VIEW_STACK: PoolIntArray = PoolIntArray([])


# BUILTINS -------------------------


const TWEEN_SPEED: float = 0.3
var _t: bool


func _ready() -> void:
	randomize()
	add_view_to_stack(Global.get("VIEW_MAP").SPLASH, {})


# METHODS -------------------------


# добавление вьюхи в стэк и отображение ее в интерфейсе
func add_view_to_stack(view: int, payload: Dictionary) -> void:
	# подгружаем новую вьюху, вставляем в экран
	var next_view: Node
	next_view = (load(Global.get("VIEWS")[view]) as PackedScene).instance()
	# если во вьюхе есть сигналы переключения вьюх, то коннектим их
	var _c: int
	if next_view.has_signal("change_view_requested"):
		_c = next_view.connect("change_view_requested", self, "_on_View_change_view_requested")
	if next_view.has_signal("remove_view_requested"):
		_c = next_view.connect("remove_view_requested", self, "_on_View_remove_view_requested")
	# добавляем в основную вьюху
	STACK_CONTAINER.add_child(next_view)
	# добавляем пейлоад во вьюху
	if not payload.empty() and next_view.get("payload") != null:
		next_view.set("payload", payload)
	VIEW_STACK.append(next_view.get_instance_id())
	# анимируем открытие вьюхи
	_t = ($Tween as Tween).interpolate_property(next_view, "modulate:a", 0.0, 1.0, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# удаление вьюхи из стэка и удаление из интерфейса
func remove_view_from_stack(payload: Dictionary) -> void:
	if not VIEW_STACK.empty():
		# дисконнектим текущую вьюху
		var view = instance_from_id(VIEW_STACK[VIEW_STACK.size() - 1])
		# добавляем пейлоад
		if not VIEW_STACK.empty() and VIEW_STACK.size() - 1 > 0:
			var prew_view = instance_from_id(VIEW_STACK[VIEW_STACK.size() - 2])
			if not payload.empty() and prew_view.get("payload") != null:
				prew_view.set("payload", payload)
		# анимируем закрытие вьюхи
		_t = ($Tween as Tween).interpolate_property(view, "modulate:a", 1.0, 0.0, TWEEN_SPEED)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
		yield($Tween, "tween_all_completed")
		# удаляем вьюху
		VIEW_STACK.remove(VIEW_STACK.size() - 1)
		if view.has_signal("change_view_requested"):
			view.disconnect("change_view_requested", self, "_on_View_change_view_requested")
		if view.has_signal("remove_view_requested"):
			view.disconnect("remove_view_requested", self, "_on_View_remove_view_requested")
		view.queue_free()


func _is_game_scene_active() -> bool:
	var game_scene
	for i in VIEW_STACK:
		var game = instance_from_id(i)
		if game.name == "Game":
			game_scene = game
	return true if game_scene != null else false


# SETGET -------------------------


# SIGNALS -------------------------


# вызывается, когда вьюха запрашивает открытие другой вьюхи
# то есть была нажата кнопка в этой вьюхе, которая должна открыть другую вьюху
func _on_View_change_view_requested(view: int, payload: Dictionary = {}) -> void:
	add_view_to_stack(view, payload)


# при нажатии кнопки назад в шапке
func _on_View_remove_view_requested(payload: Dictionary = {}) -> void:
	remove_view_from_stack(payload)


